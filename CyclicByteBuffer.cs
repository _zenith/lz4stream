//
//  Copyright 2013  Matthew Ducker
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using System;

namespace LZ4Stream
{
	/// <summary>
	/// Simple cyclic/ring data buffer.
	/// </summary>
	/// <remarks>
	/// Makes efficient use of memory.
	/// Ensure initialised capacity can hold typical use case requirement with some overflow tolerance.	
	/// </remarks>		
	public sealed class CyclicByteBuffer
	{
		public int Capacity { get; private set; }
		public int Length { get; private set; }
		
		private readonly byte[] _buffer;
		private int _head, _tail;
		
		public CyclicByteBuffer (int capacity) {
			Capacity = capacity;
			_buffer = new byte[Capacity];
		}
		
		public CyclicByteBuffer (int capacity, byte[] buffer) : this(capacity) {
			Buffer.BlockCopy(buffer, 0, _buffer, 0, buffer.Length);
			_tail += buffer.Length;
			Length += _tail;
		}
		
		public void Put (byte[] buffer) {
			Put(buffer, 0, buffer.Length);
		}
		
		public void Put (byte[] buffer, int offset, int count) {
			if (count > Capacity - Length)
				throw new InvalidOperationException("Buffer capacity insufficient for write operation. " +
				                                    "Write a smaller quantity relative to the capacity to avoid this.");
			if (_tail + count > Capacity) {
				var chunkSize = Capacity - _tail;
				Buffer.BlockCopy(buffer, offset, _buffer, _tail, chunkSize);
				_tail = 0;
				offset += chunkSize;
				count -= chunkSize;
				Length += chunkSize;
			}
			
			Buffer.BlockCopy(buffer, offset, _buffer, _tail, count);
			_tail += count;
			Length += count;
		}
		
		public void Put (byte input) {
			if (Length + 1 > Capacity) throw new InvalidOperationException("Buffer capacity insufficient for write operation.");
			
			_buffer[_tail++] = input;
			if (_tail == Capacity) _tail = 0;
			Length++;
		}
		
		public void Take (byte[] buffer) {
			Take(buffer, 0, buffer.Length);
		}
		
		public byte[] Take (int count) {
			var output = new byte[count];
			Take(output);
			return output;
		}
		
		public void Take (byte[] buffer, int offset, int count) {
			if (count > Length)
				throw new InvalidOperationException("Buffer contents insufficient for read operation. " +
				                                    "Request a smaller quantity relative to the capacity to avoid this.");
			if (buffer.Length < offset + count)
				throw new ArgumentException("Destination array too short for requested output.", "buffer");
			
			if(count == 0) return;
			
			if (_head + count > Capacity) {
				var chunkSize = Capacity - _head;
				Buffer.BlockCopy(_buffer, _head, buffer, offset, chunkSize);
				_head = 0;
				offset += chunkSize;
				count -= chunkSize;
				Length -= chunkSize;
			}
			
			Buffer.BlockCopy(_buffer, _head, buffer, offset, count);
			_head += count;
			Length -= count;
		}
		
		public byte Take () {
			if (Length == 0) throw new InvalidOperationException("Buffer contents insufficient for read operation.");
			
			Length--;
			var output = _buffer[_head++];
			if (_head == Capacity) _head = 0;
			
			return output;
		}
		
		public byte[] ToArray () {
			return Take(Length);
		}
	}
}

